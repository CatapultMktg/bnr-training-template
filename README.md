# Training template for use of git, bitbucket and heroku

## Prep

1. Get local Mac Admin
1. Install and Launch Sourcetree
1. Configure Sourcetree Account integration with BitBucket
1. Configure Sourcetree License (it's free)
1. Generate License, use "Catapult Marketing" as your organization
1. Install xcode from the OSX App Store
1. Log into BitBucket in Browser
1. Log into Heroku in Browser
1. In your home directory create a folder called "Projects"
1. You should have a text editor installed like Dreamweaver, Code or Sublime.

## High Level Process

1. Copy the template to your project
2. Create a Repo on BitBucket
1. Add your project to bitbucket
1. Do a test deploy to Heroku
1. Deploy your project to Heroku for QA, Account review and client review

## Moderate instructions

[Full Training Docs](https://catapultrpm.atlassian.net/wiki/spaces/DEV/pages/109215791/git+and+Heroku+Training+Guide) (Password protected)

1. 1. Configure finder to [show hidden files](http://www.macworld.co.uk/how-to/mac-software/how-show-hidden-files-library-folder-mac-3520878/)
1. Open Terminal
1. Paste in the two commands from the end of the artcile above
Lauching Sourcetree
1. Generate License, use Catapult Marketing as your organization
1. From bitbucket, create a new repo for your project, following the naming convention, hereto called Project X.

Split view stagintg
2. From SourceTree, clone Project X locally.
3. In Finder, open the reference repo. Copy its contents, minus the .git folder into the empty Project X folder.
4. Locally make the changes, commit the changes, push the changes to origin
5. Open Heroku dashboard.
6. Create a new App in the Catapult Marketing organization, following the naming convention.
7. In SourceTree, add a new remote, called "staging" for the Heroku App.
1. From the Heroku dashboard, make a note of the URL for the App, and add it to your local README.md
1. The username and password for the Heroku app remote is always ```cm``` and ```cm```.